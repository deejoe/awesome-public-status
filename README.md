# Awesome public status

This is a two-part list of public status pages of Internet services, either provided
by the services themselves (the preferred form for this list) or
third-party services monitoring and reporting a less rich set of
availability information for a list of Internet services.

The former set are awesome because these organizations hold themselves
accountable by providing outage information without their users having to
rely on services like those in the latter set.  

Self-reported status can be customized to a richer set of metrics, some of
which may be unique to the service and collectively to its users or
potential users.

The latter, third-party set of services are awesome because they pick up the
slack for entities that do not provide their own reports, and because they
more readily enable comparison across providers in a consistent format.

## Self-reported status

* [Adobe System Status](https://status.adobe.com/) - Adobe services status
* [Amazon AWS Service health dashboard](https://status.aws.amazon.com/)
* [Apple Support System Status](https://www.apple.com/support/systemstatus/) - Apple support status
* [Box Status](https://status.box.com/) - Box.net status
* [GitHub Status](https://www.githubstatus.com/) - GitHub status page
* [GitLab System Status](https://status.gitlab.com) - GitLab status page
* [Google GSuite Appstatus](https://www.google.com/appsstatus) - Google Applications (G Suite) online status page
* [Google Cloud Status](https://status.cloud.google.com/) - Status of Google Cloud
* [Heroku Status](https://status.heroku.com/) - Heroku status page
* [Microsoft Azure Status](https://status.azure.com/) - Microsoft Azure status page
* [Microsoft Office365 Status](https://status.office365.com/) - Microsoft Office365 status page
* [Paypal Status Page](https://www.paypal-status.com/product/production)
* [Rackspace System Status](https://status.rackspace.com/) - Rackspace hosting and services status page
* [Slack Status](https://status.slack.com/) - Slack, chat platform, status page
* [status.io](https://status.status.io/)

## More general monitoring and reporting aggregators

* [IsItDown RightNow?](https://www.isitdownrightnow.com/)
* [Down for Everyone or Just Me](https://downforeveryoneorjustme.com/)
* [Downdetector](https://downdetector.com)

This was forked from
[awesome-public-status](https://github.com/ivbeg/awesome-status-pages) to
focus strictly on public status pages for internet organizations, either direct or
third-party. 

The lists of self-hostable open source status software and of
hosted status-reporting-as-a-service have been removed.

